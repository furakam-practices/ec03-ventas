using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace VENTAS2023_MVC.Controllers
{
    [Route("[controller]")]
    public class DarBajaController : Controller
    {
        [Route("{codigo}")]
        public IActionResult Index(string codigo)
        {
            ViewBag.codigo = codigo;
            return View();
        }

        [Route("Bajar/{codigo}")]
        public async Task<IActionResult> Bajar(string codigo)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:7077/api/");
                HttpResponseMessage responseMessage = await client.PostAsync($"Articulo/dar-baja/{codigo}",null);
                if (responseMessage.IsSuccessStatusCode){
                    Console.WriteLine(responseMessage.Content.ToString());
                }else{
                    Console.WriteLine("Error: "+responseMessage.Content.ToString());
                }
            }
            return RedirectToAction("Index", "Home");
        }
    }
}