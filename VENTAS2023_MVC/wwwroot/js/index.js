function generateTable(columns, data, elementId) {
  let headers = "";
  columns.forEach((header) => {
    headers += `<th class="bg-primary text-white fw-normal text-center">${header}</th>`;
  });

  let rows = "";
  data.forEach((data) => {
    let cols = "";
    let id = "";
    Object.entries(data).forEach((entrie) => {
      if (id == "") id = entrie[1];
      if (entrie[0] === "imageUrl") {
        cols += `<td class="text-truncate text-center" style="max-width: 350px;min-width: 150px"><img src="${entrie[1]}" alt="Imagen" class="img-fluid w-25" /></td>`;
      } else {
        cols += `<td class="text-truncate text-center" style="max-width: 350px;min-width: 150px">${entrie[1]}</td>`;
      }
    });
    cols += `
    <td>
      <div class="d-flex gap-2">
        <a class="btn btn-danger" id="btnDarBaja" href="/DarBaja/${id}">Dar baja</a>
      </div>
    </td>
    `;
    rows += `<tr>${cols}</tr>`;
  });

  let htmlTable = `
  <table class="table table-light">
    <thead>
      <tr>
        ${headers}
      </tr>
    </thead>
    <tbody>
      ${rows}
    </tbody>
  </table>
  `;
  $(`#${elementId}`).html(htmlTable);
}

const columns = [
  "Codigo",
  "Nombre articulo",
  "U. Medida",
  "Precio",
  "Stock",
  "Image",
  "Actions",
];
$.get("https://localhost:7077/api/Articulo", function (data) {
  generateTable(columns, data, "table");
});

function filtrar() {
  var name = $("#buscarTxt").val();
  console.log(name);
  const columns = [
    "Codigo",
    "Nombre articulo",
    "U. Medida",
    "Precio",
    "Stock",
    "Image",
    "Actions",
  ];
  $.get(`https://localhost:7077/api/Articulo/by-nombre?nombre=${name}`, function (data) {
    generateTable(columns, data, "table");
  });
}

