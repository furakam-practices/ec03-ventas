USE master
GO

-- descomentar para reinciar base de datos
/* ALTER DATABASE [BDVENTAS2023_WEBAPI] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO

DROP DATABASE [BDVENTAS2023_WEBAPI];
GO */
---

CREATE DATABASE [BDVENTAS2023_WEBAPI]
GO

USE [BDVENTAS2023_WEBAPI]
GO

CREATE TABLE tb_articulos(
cod_art char (5) primary key,
nom_art varchar(50) NOT NULL,
uni_med varchar(50) NOT NULL,
pre_art decimal(8,2) NOT NULL,
stk_art int NOT NULL,
de_baja bit default 0 NOT NULL,
image_url nvarchar(MAX) NOT NULL
)
GO
CREATE TABLE tb_articulos_baja (
cod_art char (5) NOT NULL,
fecha_baja date default(getdate()) NOT NULL,
constraint pk_tb_articulos_baja 
 Primary key (cod_art, fecha_baja) ,
 constraint fk_tb_art_baja_cod_art Foreign Key(cod_art)
 References tb_articulos(cod_art)
)
GO
CREATE TABLE tb_articulos_liquidacion (
num_reg int identity(1,1) NOT NULL,
cod_art char (5) NOT NULL,
unidades_liquidar int NOT NULL ,
precio_liquidar decimal(7,2) NOT NULL,
 constraint pk_tb_art_liquidacion Primary Key(num_reg),
constraint fk_tb_art_liqui_cod_art Foreign Key(cod_art)
References tb_articulos(cod_art)
)
GO


--data fake
INSERT INTO tb_articulos (cod_art, nom_art, uni_med, pre_art, stk_art, de_baja, image_url)
VALUES
('A001', 'Camiseta', 'Unidad', 20.00, 100, 0, 'https://frutocuatro.com/wp-content/uploads/2018/05/camiseta-64000-azul-marino-frente.jpg'),
('B002', 'Pantalón', 'Unidad', 35.50, 0, 0, 'https://leblogdemonsieur.com/wp-content/uploads/2020/08/pantalon-cargo-homme-1.jpg?x59032'),
('C003', 'Zapatos deportivos', 'Par', 65.00, 0, 0, 'https://www.lidl.es/media/product/0/0/0/3/1/1/8/zapatillas-deportivas-mujer-zoom.jpg'),
('D004', 'Gorra', 'Unidad', 12.75, 120, 0, 'https://caphunters.es/13526-large_default/gorra-plana-roja-ajustada-59fifty-essential-de-new-york-yankees-mlb-de-new-era.jpg'),
('E005', 'Bolso de mano', 'Unidad', 30.00, 60, 0, 'https://www.artigianodelcuo.io/1669-thickbox_default/bolso-de-mano-pequenas-de-cuero-beige-artisanales.jpg');