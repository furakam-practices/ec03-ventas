using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VENTAS2023_WEBAPI.Models.Dto
{
    public class ArticuloDto
    {
        public string CodArt { get; set; } = null!;

        public string NomArt { get; set; } = null!;

        public string UniMed { get; set; } = null!;

        public decimal PreArt { get; set; }

        public int StkArt { get; set; }

        public string ImageUrl { get; set; } = null!;
    }
}