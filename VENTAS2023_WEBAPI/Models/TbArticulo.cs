﻿using System;
using System.Collections.Generic;

namespace VENTAS2023_WEBAPI.Models;

public partial class TbArticulo
{
    public string CodArt { get; set; } = null!;

    public string NomArt { get; set; } = null!;

    public string UniMed { get; set; } = null!;

    public decimal PreArt { get; set; }

    public int StkArt { get; set; }

    public bool DeBaja { get; set; }

    public string ImageUrl { get; set; } = null!;

    public virtual ICollection<TbArticulosBaja> TbArticulosBajas { get; set; } = new List<TbArticulosBaja>();

    public virtual ICollection<TbArticulosLiquidacion> TbArticulosLiquidacions { get; set; } = new List<TbArticulosLiquidacion>();
}
