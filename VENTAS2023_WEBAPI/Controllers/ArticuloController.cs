using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VENTAS2023_WEBAPI.Models;
using VENTAS2023_WEBAPI.Models.Dto;
using Microsoft.EntityFrameworkCore;

namespace VENTAS2023_WEBAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ArticuloController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly Bdventas2023WebapiContext _context;

        public ArticuloController(IMapper mapper, Bdventas2023WebapiContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ArticuloDto>>> ListarArticulos()
        {
            var response = Enumerable.Empty<ArticuloDto>();
            try
            {
                var articulos = await _context.TbArticulos.Where(a => a.DeBaja == false).ToListAsync();
                response = articulos.Select(a => _mapper.Map<ArticuloDto>(a)).ToList();
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
            return Ok(response);
        }

        [HttpGet("by-nombre")]
        public async Task<ActionResult<IEnumerable<ArticuloDto>>> FiltrarPorNombre(string? nombre)
        {
            var response = Enumerable.Empty<ArticuloDto>();
            try
            {
                if (nombre == null){
                    nombre = "";
                }

                var articulos = await _context.TbArticulos.Where(a => a.DeBaja == false && a.NomArt.StartsWith(nombre)).ToListAsync();

                response = articulos.Select(a => _mapper.Map<ArticuloDto>(a)).ToList();
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
            return Ok(response);
        }

        [HttpGet("by-codigo")]
        public async Task<ActionResult<ArticuloDto>> FiltrarPorCodigo(string codigo)
        {
            var response = new ArticuloDto();
            try
            {
                var articulo = await _context.TbArticulos.Where(a => a.DeBaja == false && a.CodArt == codigo).FirstOrDefaultAsync();

                response = _mapper.Map<ArticuloDto>(articulo);
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
            return Ok(response);
        }

        [HttpPost("dar-baja/{codigo}")]
        public async Task<ActionResult<string>> DarBajaPorCodigo(string codigo)
        {
            var response = "";
            try
            {
                var articulo = await _context.TbArticulos.Where(a => a.DeBaja == false && a.CodArt == codigo).FirstOrDefaultAsync() ?? throw new Exception("Artículo no encontrado o ya dado de baja");

                var baja = new TbArticulosBaja{
                    CodArt = articulo.CodArt
                };
                await _context.TbArticulosBajas.AddAsync(baja);

                if (articulo.StkArt > 0){
                    var liquidacion = new TbArticulosLiquidacion{
                        CodArt = articulo.CodArt,
                        UnidadesLiquidar = articulo.StkArt,
                        PrecioLiquidar = articulo.PreArt
                    };
                    await _context.AddAsync(liquidacion);
                }
                articulo.DeBaja = true;
                await _context.SaveChangesAsync();
                response = "Dado de baja con éxito";
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
            return Ok(response);
        }
    }
}