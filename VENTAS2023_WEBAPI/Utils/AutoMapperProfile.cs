using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using VENTAS2023_WEBAPI.Models;
using VENTAS2023_WEBAPI.Models.Dto;

namespace VENTAS2023_WEBAPI.Utils
{
    public class AutoMapperProfile:Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<TbArticulo, ArticuloDto>();
        }
    }
}